﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Registration.aspx.cs" Inherits="User_Management.Registration" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            text-align: left;
            font-size: xx-large;
        }
        .auto-style2 {
            width: 72%;
        }
        .auto-style3 {
            text-align: left;
            width: 130px;
        }
        .auto-style4 {
            text-align: left;
            margin-left: 40px;
        }
        .auto-style7 {
            text-align: left;
            width: 130px;
            height: 23px;
        }
        .auto-style8 {
            height: 23px;
        }
        .auto-style10 {
            text-align: left;
            width: 130px;
            height: 26px;
        }
        .auto-style11 {
            height: 26px;
            text-align: left;
        }
        .auto-style14 {
            text-align: left;
        }
        .auto-style15 {
            width: 128px;
        }
        .auto-style16 {
            width: 178px;
        }
        .auto-style17 {
            text-align: left;
            margin-left: 40px;
            width: 224px;
        }
        .auto-style18 {
            text-align: left;
            width: 224px;
        }
        .auto-style19 {
            height: 23px;
            width: 224px;
        }
        .auto-style20 {
            height: 26px;
            text-align: left;
            width: 224px;
        }
        .auto-style21 {
            text-align: left;
            width: 130px;
            height: 40px;
        }
        .auto-style22 {
            text-align: left;
            width: 224px;
            height: 40px;
        }
        .auto-style23 {
            text-align: left;
            height: 40px;
        }
        .auto-style24 {
            text-align: left;
            width: 224px;
            height: 23px;
        }
        .auto-style25 {
            text-align: left;
            height: 23px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="auto-style1">
            <strong>Registration Page<br />
            <br />
            </strong>
        </div>
        <table class="auto-style2">
            <tr>
                <td class="auto-style3">First Name</td>
                <td class="auto-style17">
                    <asp:TextBox ID="TextBoxFirstName" runat="server" Width="180px"></asp:TextBox>
                </td>
                <td class="auto-style4">
                    &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxFirstName" ErrorMessage="Please Enter First Name" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">Last Name</td>
                <td class="auto-style18">
                    <asp:TextBox ID="TextBoxLastName" runat="server" Width="180px"></asp:TextBox>
                </td>
                <td class="auto-style14">
                    &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBoxLastName" ErrorMessage="Please Enter Last Name" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">Contact</td>
                <td class="auto-style18">
                    <asp:TextBox ID="TextBoxContact" runat="server" Width="180px"></asp:TextBox>
                </td>
                <td class="auto-style14">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBoxContact" ErrorMessage="Please Enter Contact Number" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">Gender</td>
                <td class="auto-style18">
                    <asp:DropDownList ID="DropDownListGender" runat="server" Width="184px" Height="20px">
                        <asp:ListItem>Male</asp:ListItem>
                        <asp:ListItem>Female</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td class="auto-style14">
                    &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="DropDownListGender" ErrorMessage="Please Select Gender" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style21">Address</td>
                <td class="auto-style22">
                    <textarea id="TextAreaAddress" class="auto-style16" name="S1" rows="2"></textarea></td>
                <td class="auto-style23">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style7">
                    <hr class="auto-style15" />
                </td>
                <td class="auto-style19">
                    <hr />
                </td>
                <td class="auto-style8">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style10">Username</td>
                <td class="auto-style20">
                    <asp:TextBox ID="TextBoxUserName" runat="server" Width="180px"></asp:TextBox>
                </td>
                <td class="auto-style11">&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="TextBoxUserName" ErrorMessage="Please Enter User Name" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style10">ISAdmin</td>
                <td class="auto-style20">
                    <asp:RadioButton ID="RadioButtonISAdminEnable" runat="server" />
                </td>
                <td class="auto-style11">&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">Password</td>
                <td class="auto-style18">
                    <asp:TextBox ID="TextBoxPassword" runat="server" Width="180px" TextMode="Password"></asp:TextBox>
                </td>
                <td class="auto-style14">&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="TextBoxPassword" ErrorMessage="Please Enter Password" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">Confirm Password</td>
                <td class="auto-style18">
                    <asp:TextBox ID="TextBoxConfirmPassword" runat="server" Width="180px" TextMode="Password"></asp:TextBox>
                </td>
                <td class="auto-style14">&nbsp;&nbsp;&nbsp;&nbsp; <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="TextBoxPassword" ControlToValidate="TextBoxConfirmPassword" ErrorMessage="Password &amp; Confirm Password mismatched" ForeColor="Red"></asp:CompareValidator>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style7">
                </td>
                <td class="auto-style24"></td>
                <td class="auto-style25"></td>
            </tr>
            <tr>
                <td class="auto-style3">
                    <asp:Button ID="Button_Submit" runat="server" Text="Submit" OnClick="Button_Submit_Click" />
                </td>
                <td class="auto-style18">&nbsp;</td>
                <td class="auto-style14">
                    &nbsp;</td>
            </tr>
        </table>
    </form>
</body>
</html>
