﻿CREATE TABLE [dbo].[q_user] (
    [firstname] VARCHAR (50)  NOT NULL,
    [lastname]  VARCHAR (50)  NULL,
    [contact]   VARCHAR (10)  NOT NULL,
    [gender]    VARCHAR (10)  NOT NULL,
    [address]   VARCHAR (250) NULL,
    [username]  VARCHAR (15)  NOT NULL,
    [password]  VARCHAR (15)  NOT NULL,
    PRIMARY KEY CLUSTERED ([username] ASC)
);

